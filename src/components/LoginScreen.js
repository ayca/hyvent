import React, { Component } from 'react';
import {
    View,
    TextInput,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';

import { connect } from 'react-redux';
import firebase from 'firebase';
import LoginButton from './LoginButton';
import Spinner from './Spinner';
import { login,passwordChange,emailChange } from '../actions';
import hyventLogo from '../images/hyventlogo.png';

class LoginScreen extends Component {

    clickLogin = () => {
        this.props.login(this.props.email,this.props.password)
    }


    renderButton = () => {  
        if (this.props.loading == true) {
            console.log('spinner');
            return <Spinner size = 'large'/>;        
        }else{
            console.log('Button');
            return <LoginButton onPress={this.clickLogin}> Giriş </LoginButton>;                
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.nav) {
            this.props.navigation.navigate('MainScreen');
        }
    }
    render() {
        return(
            <View style = {styles.mainViewStyle}>
                <View style = {styles.logoViewStyle}>
                    <Image
                        style={styles.logoStyle}
                        source={hyventLogo}
                    />
                </View>
                <View style = {styles.loginViewStyle}>
                    <TextInput 
                        placeholder= 'e-mail'
                        underlineColorAndroid = 'white'
                        style= {styles.emailInput}
                        onChangeText = {(email) => this.props.emailChange(email)}
                    />
                    <TextInput 
                        secureTextEntry
                        placeholder= 'parola'
                        underlineColorAndroid = 'white'                        
                        style= {styles.passwordInput}
                        onChangeText = {(password) => this.props.passwordChange(password)}
                    />

                        {this.renderButton()}
                    
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate('RegisterScreen')} style = {{marginTop: '4%'}}> 
                        <Text style = {{color:'white'}}>Hemen Kaydol</Text>     
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }
}

const styles = {
    logoStyle: {
        height:72.5,
        width:200,
        marginBottom:12
    },
    mainViewStyle: {
        justifyContent:'center',
        alignItems:'center',
        width: '100%',
        height: '100%',
        backgroundColor: '#26d591', 
    },
    logoViewStyle: {
        height: '35%',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    loginViewStyle: {
        justifyContent:'center',
        alignItems:'center',
        height: '65%',
        width: '100%',
        //backgroundColor:'red'
    },
    emailInput: {
        width: '75%',
        height: 40,
        backgroundColor:'white',
        borderRadius:6,
        borderColor: 'white',
        borderWidth: 1.2,
        marginBottom: '5%',
        textAlign: 'center'
    },
    passwordInput: {
        width: '75%',
        height: 40,
        backgroundColor:'white',
        borderRadius: 6,
        borderColor: 'white',
        borderWidth: 1.2,
        marginBottom: '5%',
        textAlign: 'center'
    },
}

const mapStateToProps = ({ LoginResponse }) => {
    const { email, password, loading, nav } = LoginResponse;
    return {
        email,
        password,
        loading,
        nav
    };
};

export default connect(mapStateToProps, {login, emailChange,passwordChange})(LoginScreen);