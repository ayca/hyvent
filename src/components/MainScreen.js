import React, { Component } from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import NoteListScreen from './NoteListScreen';
import ProfileScreen from './ProfileScreen';
import AddNoteScreen from './AddNoteScreen';
import DiscoverScreen from './DiscoverScreen';
import CommonNoteListScreen from './CommonNoteListScreen';
import Icon from 'react-native-vector-icons/Feather';

//const addIcon = (<Icon name="rocket" size={30} color="#900" />);

class MainScreen extends Component {

    render() {
        return(
          <View style = {{flex:1}}>
            <createBottomTabNavigator/>
          </View>
            );
    }
}

export default createBottomTabNavigator({
    DiscoverScreen: DiscoverScreen,
    NoteListScreen: NoteListScreen,
    AddNoteScreen: AddNoteScreen,
    CommonNoteListScreen: CommonNoteListScreen,
    ProfileScreen: ProfileScreen
  }, {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'NoteListScreen') {
          iconName = `user`
        } else if (routeName === 'AddNoteScreen') {
          iconName = `plus-circle`;
        } else if (routeName === 'ProfileScreen') {
          iconName = `settings`
        } else if (routeName === 'CommonNoteListScreen') {
          iconName = `users`
        } else if (routeName === 'DiscoverScreen') {
          iconName = `search`
        }
        return <Icon name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      tabStyle: {
        
      },
      showLabel: false,
      labelStyle: 'none',
      activeTintColor: '#26d591',
      inactiveTintColor: 'gray',
    },
  }
);
