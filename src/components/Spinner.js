import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = ({ size }) => {
    return(
        <View style={styles.spinnerStyle}>
            <ActivityIndicator color='white' size={ size || 'small'} />
        </View>
    );
};
const styles = {
    spinnerStyle: {
        height: 40,
        justifyContent: 'center',
    }
};
export default Spinner;
