import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { createStackNavigator } from 'react-navigation';

import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';

import reducers from './src/reducers';
import LoginScreen from './src/components/LoginScreen';
import RegisterScreen from './src/components/RegisterScreen';
import MainScreen from './src/components/MainScreen';
import firebase from 'firebase';

export default class App extends Component {

  componentWillMount() {
    firebase.initializeApp({
        apiKey: 'AIzaSyBqy4p0b7XULeGA3g2jpQg0bsNON72y48w',
        authDomain: 'hyvent-6b82d.firebaseapp.com',
        databaseURL: 'https://hyvent-6b82d.firebaseio.com',
        projectId: 'hyvent-6b82d',
        storageBucket: 'hyvent-6b82d.appspot.com',
        messagingSenderId: '646105924549'
      });
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk))
    return (
      <Provider store= {store}>
        <RootStack/>
      </Provider>
    );
  }
}

const RootStack = createStackNavigator({
  LoginScreen: {
    screen: LoginScreen
  },
  RegisterScreen: {
    screen: RegisterScreen
  },
  MainScreen: {
    screen: MainScreen
  }
},
{headerMode: 'none'},
{initialRouteName: 'LoginScreen'});